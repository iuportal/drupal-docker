Vamos a construir una nueva dockerización para un entorno de producción con los siguientes requisitos:
- [ ] El contenedor se desplegará en una ruta cualquiera. Para este caso, consideraremos que el contenedor se despliega en /var/sites/mysite 
- [ ] El servidor web y la base de datos no estarán dockerizados. 
- El servidor web será un nginx (tener en cuenta la configuración de nginx de la anterior dockerización: fue necesario poner una regla de rewrite para las imágenes porque los estilos de imágenes no existen hasta que se le piden la primera vez a drupal que los genera al recibir la petición y los almacena, para siguientes peticiones, en el directorio public://styles)
- Habrá un managed mariadb externo en vez de contenido en un container en una red interna de docker. Al ser un servicio externo, se soluciona el problema de la persistencia de la base de datos, backups, etc. y el rendimiento será mejor o más fácilmente mejorable. Habrá que tener en cuenta criterios de seguridad ya que deberá estar en una red accesible desde los contenedores en vez de en una red interna.
- [ ] El directorio public:// debe ser un volumen físico de la máquina
- No queremos que el directorio public: se encuentre, como en las instalaciones habituales de drupal, en sites/default/files porque las urls de los ficheros son muy largas (www.example.com/sites/default/files/styles/public/16x9/imagen.png) sino en files para ahorrarnos parte de la URL (www.example.com/files/styles/public/16x9/imagen.png) por lo que habrá que tocar el fichero settings.php para modificar esta propiedad.
- El directorio files será en el docker.compose.xml un volumen físico. Algo así:
```
    volumes:
      - ./files:/var/www/html/web/files
```

- El directorio files debe ser propiedad del usuario www-data. Pensar si debe existir un grupo con permisos de escritura al que pertenezca el usuario de despliegues.
- Se configurará para que los recortes de imagen (styles) se generen en files/styles, los css comprimidos en files/css y los javascript comprimidos en files/js. Como files es un volumen fisico queda también solucionado el problema de la persistencia frente a rebuild de contenedores: No se tendrán que generar esos ficheros de nuevo.
- En caso de optar por tener varios contenedores corriendo drupal con un balanceador delante de ellos, files sería un montaje de un NFS u otro tipo de repositorio compartido.
- [ ] Las variables de conexión a la base de datos deben llegar del fichero de environment.
- La configuración de la base de datos (y otras) se realizará en el fichero .env:
```
### PROJECT SETTINGS

PROJECT_NAME=[prefijo del proyecto]
PROJECT_BASE_URL=[URL del portal]

DRUPAL_DB_ROOT_PASSWORD=[root password]
DRUPAL_DB_NAME=[Nombre de la base de datos]
DRUPAL_DB_USER=[Usuario de la base de datos]
DRUPAL_DB_PASSWORD=[password]
DRUPAL_DB_HOST=[Nombre de la máquina de la db externa]
DRUPAL_DB_PORT=[Listener port de la db externa]
```

- Se modificará el settings.php para recoger esas variables de entorno:
```
$databases = array (
  'default' => array (
    'default' => array (
      'database' => $envs['DRUPAL_DB_NAME'] ?? '',
      'username' => $envs['DRUPAL_DB_USER'] ?? '',
      'password' => $envs['DRUPAL_DB_PASSWORD'] ?? '',
      'host' => $envs['DRUPAL_DB_HOST'] ?? '',
      'port' => $envs['DRUPAL_DB_PORT'] ?? '',
      'driver' => 'mysql',
      'prefix' => 'drupal_',
      'charset' => 'utf8mb4',
      'collation' => 'utf8mb4_general_ci',
      'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    ),
  ),
); 
```
Aunque creo que prefiero este otro settings más sencillo y más docker-compose:
```
// Config sync directory.
$config_directories['sync'] = '../config/sync';

// Hash salt.
$settings['hash_salt'] = getenv('DRUPAL_HASH_SALT');

// Disallow access to update.php by anonymous users.
$settings['update_free_access'] = FALSE;

// Other helpful settings.
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';

// Database connection.
$databases['default']['default'] = [
  'database' => getenv('DRUPAL_DATABASE_NAME'),
  'username' => getenv('DRUPAL_DATABASE_USERNAME'),
  'password' => getenv('DRUPAL_DATABASE_PASSWORD'),
  'prefix' => '',
  'host' => getenv('DRUPAL_DATABASE_HOST'),
  'port' => getenv('DRUPAL_DATABASE_PORT'),
  'namespace' => 'Drupal\Core\Database\Driver\mysql',
  'driver' => 'mysql',
]
```
que se podrían pasar en el yml
```
services:
  drupal:
    image: infernial/iudrupal:v1
    container_name: "${PROJECT_NAME}_drup"
    environment:
      DRUPAL_DATABASE_HOST: 'mysql'
      [...]
      DRUPAL_HASH_SALT: 'fe918c992fb1bcfa01f32303c8b21f3d0a0'
```


- [ ] Se debe ejecutar el cron periodicamente

- [ ] Tomar éste como ejemplo de Dockerfile https://raw.githubusercontent.com/docker-library/drupal/06872ee93dd032db13fa34b6cb868f8cf3b7f6d1/9.0/fpm-alpine3.12/Dockerfile

- [ ] La estructura de despliegue (tras hacer un despliegue del sitio será) será:
```
/var/sites/mysite/
-----------------files
-----------------.env
-----------------otros
```


- [ ] Apuntes
- Usar config split y config ignore https://www.drupal.org/project/config_ignore/releases/8.x-3.x-dev
- 12 factor apps https://12factor.net/
- “12-factor“ize drupal https://www.shapeblock.com/using-drupal-and-docker-in-production/
